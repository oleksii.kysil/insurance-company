using System;
using System.Collections.Generic;
using InsuranceCompany.Company;
using InsuranceCompany.Models;
using Xunit;

namespace InsuranceCompanyTests.Company
{
    public class InsuranceCompanyTest
    {
        private readonly IInsuranceCompany _insuranceCompany;
        private const string DefaultRiskName = "test_risk";
        private const decimal DefaultYearlyPrice = 365;
        private const string NewAvailableRisk = "new_risk";
        private const string DefaultCompanyName = "test_company";
        private const string ExistingObjectName = "existing_object_name";
        private const string NewObjectName = "new_object_name";

        private static readonly IList<Risk> DefaultRisks = new List<Risk>
            {new Risk {Name = DefaultRiskName, YearlyPrice = DefaultYearlyPrice}};

        public InsuranceCompanyTest()
        {
            _insuranceCompany = new InsuranceCompany.Company.InsuranceCompany(DefaultCompanyName)
            {
                AvailableRisks = new List<Risk>
                {
                    new Risk {Name = DefaultRiskName, YearlyPrice = DefaultYearlyPrice},
                    new Risk {Name = NewAvailableRisk, YearlyPrice = DefaultYearlyPrice}
                }
            };
            _insuranceCompany.SellPolicy(ExistingObjectName, DateTimeOffset.UtcNow.AddDays(1).DateTime, 12,
                new List<Risk>(DefaultRisks));
        }

        [Fact]
        public void TestSimpleCtor()
        {
            var instance = new InsuranceCompany.Company.InsuranceCompany(DefaultCompanyName);
            Assert.NotNull(instance);
            Assert.Equal(DefaultCompanyName, instance.Name);
            Assert.NotNull(instance.AvailableRisks);
            Assert.Empty(instance.AvailableRisks);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("   ")]
        public void TestCtorFailure(string name)
        {
            Assert.ThrowsAny<ArgumentException>(() => new InsuranceCompany.Company.InsuranceCompany(name));
        }

        [Fact]
        public void TestFullCtor()
        {
            var instance = new InsuranceCompany.Company.InsuranceCompany(DefaultCompanyName,
                new List<Risk> {new Risk {Name = DefaultRiskName, YearlyPrice = DefaultYearlyPrice}});
            Assert.NotNull(instance);
            Assert.Equal(DefaultCompanyName, instance.Name);
            Assert.NotNull(instance.AvailableRisks);
            Assert.NotEmpty(instance.AvailableRisks);
        }

        [Theory]
        [MemberData(nameof(SellPolicyWrongParameters))]
        public void TestSellPolicyWrongParameters(string name, DateTime validFrom, short month, IList<Risk> risks)
        {
            Assert.ThrowsAny<ArgumentException>(() => _insuranceCompany.SellPolicy(name, validFrom, month, risks));
        }

        [Fact]
        public void TestSellPolicy()
        {
            var dateNow = DateTimeOffset.UtcNow;
            var result = _insuranceCompany.SellPolicy(NewObjectName, dateNow.AddDays(2).DateTime, 12,
                DefaultRisks);
            Assert.NotNull(result);
            Assert.Equal(dateNow.AddDays(2).DateTime, result.ValidFrom);
            Assert.Equal(dateNow.AddDays(2).AddMonths(12).DateTime, result.ValidTill);
            Assert.Equal(NewObjectName, result.NameOfInsuredObject);
            Assert.True(result.Premium <= 366);

            result = _insuranceCompany.SellPolicy(NewObjectName, dateNow.AddYears(2).DateTime, 12,
                DefaultRisks);
            Assert.NotNull(result);
            Assert.Equal(dateNow.AddYears(2).DateTime, result.ValidFrom);
            Assert.Equal(dateNow.AddYears(2).AddMonths(12).DateTime, result.ValidTill);
            Assert.Equal(NewObjectName, result.NameOfInsuredObject);
            Assert.True(result.Premium <= 366);
            Assert.True(result.Premium >= 365);
        }

        [Theory]
        [InlineData(null)]
        [InlineData(NewObjectName)]
        public void TestGetPolicyFailure(string objectName)
        {
            Assert.ThrowsAny<ArgumentException>(() => _insuranceCompany.GetPolicy(objectName, DateTime.UtcNow));
        }

        [Fact]
        public void TestGetPolicyNoPolicy()
        {
            var result = _insuranceCompany.GetPolicy(ExistingObjectName, DateTimeOffset.UtcNow.AddYears(12).DateTime);
            Assert.Null(result);
        }

        [Fact]
        public void TestGetPolicy()
        {
            var result = _insuranceCompany.GetPolicy(ExistingObjectName, DateTimeOffset.UtcNow.AddDays(2).DateTime);
            Assert.NotNull(result);
            Assert.Equal(ExistingObjectName, result.NameOfInsuredObject);
        }

        [Theory]
        [MemberData(nameof(AddRiskExceptionParams))]
        public void TestAddRiskFailure(string objectName, DateTime effectiveDate, Risk risk, DateTime validFrom)
        {
            Assert.ThrowsAny<ArgumentException>(() =>
                _insuranceCompany.AddRisk(objectName, risk, validFrom, effectiveDate));
        }

        [Fact]
        public void TestAddRisk()
        {
            _insuranceCompany.AddRisk(ExistingObjectName,
                new Risk {Name = NewAvailableRisk, YearlyPrice = DefaultYearlyPrice},
                DateTimeOffset.UtcNow.AddDays(3).DateTime,
                DateTimeOffset.UtcNow.AddDays(2).DateTime);
            var result = _insuranceCompany.GetPolicy(ExistingObjectName, DateTimeOffset.UtcNow.AddDays(2).DateTime);
            Assert.Equal(2, result.InsuredRisks.Count);
            Assert.True(result.Premium < 730);
            Assert.True(result.Premium >= 728);
        }

        [Fact]
        public void TestRemoveRisk()
        {
            var risk = new Risk {Name = NewAvailableRisk, YearlyPrice = DefaultYearlyPrice};
            var effectiveDate = DateTimeOffset.UtcNow.AddDays(2).DateTime;
            var dateNow = DateTimeOffset.UtcNow;
            _insuranceCompany.AddRisk(ExistingObjectName, risk, dateNow.AddDays(3).DateTime,
                effectiveDate);
            _insuranceCompany.RemoveRisk(ExistingObjectName, risk, dateNow.AddDays(4).DateTime, effectiveDate);
            var result = _insuranceCompany.GetPolicy(ExistingObjectName, effectiveDate);
            Assert.Equal(1, result.InsuredRisks.Count);
            Assert.True(result.Premium < 366);
            Assert.True(result.Premium >= 365);
        }

        [Theory]
        [MemberData(nameof(RemoveRiskExceptionParams))]
        public void TestRemoveRiskFailure(string objectName, DateTime effectiveDate, Risk risk, DateTime validTill)
        {
            var newRisk = new Risk {Name = NewAvailableRisk, YearlyPrice = DefaultYearlyPrice};
            var newEffectiveDate = DateTimeOffset.UtcNow.AddDays(2).DateTime;
            var dateNow = DateTimeOffset.UtcNow;
            _insuranceCompany.AddRisk(ExistingObjectName, newRisk, dateNow.AddDays(3).DateTime,
                newEffectiveDate);
            Assert.ThrowsAny<ArgumentException>(() =>
                _insuranceCompany.RemoveRisk(objectName, risk, validTill, effectiveDate));
        }

        public static IEnumerable<object[]> SellPolicyWrongParameters => new List<object[]>
        {
            new object[]
            {
                null, DateTimeOffset.UtcNow.AddDays(1).DateTime, 10, DefaultRisks
            },
            new object[]
            {
                ExistingObjectName, DateTimeOffset.UtcNow.AddDays(1).DateTime, 10, DefaultRisks
            },
            new object[]
            {
                NewObjectName, DateTimeOffset.UtcNow.Subtract(TimeSpan.FromDays(2)).DateTime, 10, DefaultRisks
            },
            new object[]
            {
                NewObjectName, DateTimeOffset.UtcNow.AddDays(1).DateTime, -10, DefaultRisks
            },
            new object[]
            {
                NewObjectName, DateTimeOffset.UtcNow.AddDays(1).DateTime, 10,
                new List<Risk> {new Risk {Name = "wrong_risk", YearlyPrice = 100500}}
            }
        };

        public static IEnumerable<object[]> AddRiskExceptionParams => new List<object[]>
        {
            new object[]
            {
                null, DateTimeOffset.UtcNow.DateTime,
                new Risk {Name = NewAvailableRisk, YearlyPrice = DefaultYearlyPrice}, DateTimeOffset.UtcNow.AddDays(1)
            },
            new object[]
            {
                NewObjectName, DateTimeOffset.UtcNow.DateTime,
                new Risk {Name = NewAvailableRisk, YearlyPrice = DefaultYearlyPrice}, DateTimeOffset.UtcNow.AddDays(1)
            },
            new object[]
            {
                ExistingObjectName, DateTimeOffset.UtcNow.AddYears(12).DateTime,
                new Risk {Name = NewAvailableRisk, YearlyPrice = DefaultYearlyPrice}, DateTimeOffset.UtcNow.AddDays(1)
            },
            new object[]
            {
                ExistingObjectName, DateTimeOffset.UtcNow.AddDays(3).DateTime,
                new Risk {Name = "blabla", YearlyPrice = DefaultYearlyPrice}, DateTimeOffset.UtcNow.AddDays(1)
            },
            new object[]
            {
                ExistingObjectName, DateTimeOffset.UtcNow.AddDays(3).DateTime,
                new Risk {Name = NewAvailableRisk, YearlyPrice = DefaultYearlyPrice},
                DateTimeOffset.UtcNow.Subtract(TimeSpan.FromDays(2)).DateTime
            }
        };

        public static IEnumerable<object[]> RemoveRiskExceptionParams => new List<object[]>
        {
            new object[]
            {
                null, DateTimeOffset.UtcNow.DateTime,
                new Risk {Name = DefaultRiskName, YearlyPrice = DefaultYearlyPrice}, DateTimeOffset.UtcNow.AddDays(1)
            },
            new object[]
            {
                NewObjectName, DateTimeOffset.UtcNow.DateTime,
                new Risk {Name = DefaultRiskName, YearlyPrice = DefaultYearlyPrice}, DateTimeOffset.UtcNow.AddDays(1)
            },
            new object[]
            {
                ExistingObjectName, DateTimeOffset.UtcNow.AddYears(12).DateTime,
                new Risk {Name = DefaultRiskName, YearlyPrice = DefaultYearlyPrice}, DateTimeOffset.UtcNow.AddDays(1)
            },
            new object[]
            {
                ExistingObjectName, DateTimeOffset.UtcNow.AddDays(3).DateTime,
                new Risk {Name = "blabla", YearlyPrice = DefaultYearlyPrice}, DateTimeOffset.UtcNow.AddDays(1)
            },
            new object[]
            {
                ExistingObjectName, DateTimeOffset.UtcNow.AddDays(3).DateTime,
                new Risk {Name = NewAvailableRisk, YearlyPrice = DefaultYearlyPrice}, DateTimeOffset.UtcNow.AddDays(1)
            },
            new object[]
            {
                ExistingObjectName, DateTimeOffset.UtcNow.AddYears(12).DateTime,
                new Risk {Name = DefaultRiskName, YearlyPrice = DefaultYearlyPrice},
                DateTimeOffset.UtcNow.Subtract(TimeSpan.FromDays(3)).DateTime
            }
        };
    }
}