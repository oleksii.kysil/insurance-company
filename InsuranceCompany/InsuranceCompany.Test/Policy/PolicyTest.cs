using System;
using System.Collections.Generic;
using InsuranceCompany.Models;
using Xunit;

namespace InsuranceCompanyTests.Policy
{
    public class PolicyTest
    {
        [Fact]
        public void TestEmptyCtor()
        {
            var policy = new InsuranceCompany.Policy.Policy();
            Assert.NotNull(policy.NameOfInsuredObject);
            Assert.Empty(policy.NameOfInsuredObject);
            Assert.True(policy.ValidFrom <= DateTime.UtcNow);
            Assert.True(policy.ValidTill > DateTime.UtcNow);
            Assert.NotNull(policy.InsuredRisks);
            Assert.Empty(policy.InsuredRisks);
            Assert.True(policy.Premium == 0);
        }

        [Fact]
        public void TestPreInitializedCtor()
        {
            var policy = new InsuranceCompany.Policy.Policy("test", DateTime.UtcNow,
                DateTimeOffset.UtcNow.AddDays(3).DateTime,
                new List<Risk> {new Risk {Name = "test_risk", YearlyPrice = 365}});
            Assert.NotNull(policy.NameOfInsuredObject);
            Assert.NotEmpty(policy.NameOfInsuredObject);
            Assert.Equal("test", policy.NameOfInsuredObject);
            Assert.True(policy.ValidFrom <= DateTime.UtcNow);
            Assert.True(policy.ValidTill > DateTime.UtcNow);
            Assert.NotNull(policy.InsuredRisks);
            Assert.NotEmpty(policy.InsuredRisks);
            Assert.True(policy.Premium == 0);
        }

        [Theory]
        [MemberData(nameof(InitializedConstructorParams))]
        public void TestPreInitializedCtorFailure(string name, DateTime fromDate, DateTime tillDate, IList<Risk> risks)
        {
            Assert.ThrowsAny<ArgumentException>(() =>
                new InsuranceCompany.Policy.Policy(name, fromDate, tillDate, risks));
        }

        [Fact]
        public void TestFullCtor()
        {
            var policy = new InsuranceCompany.Policy.Policy("test", DateTime.UtcNow,
                DateTimeOffset.UtcNow.AddDays(2).DateTime, 100500,
                new List<Risk> {new Risk {Name = "test_risk", YearlyPrice = 365}});
            Assert.NotNull(policy.NameOfInsuredObject);
            Assert.NotEmpty(policy.NameOfInsuredObject);
            Assert.Equal("test", policy.NameOfInsuredObject);
            Assert.True(policy.ValidFrom <= DateTime.UtcNow);
            Assert.True(policy.ValidTill > DateTime.UtcNow);
            Assert.NotNull(policy.InsuredRisks);
            Assert.NotEmpty(policy.InsuredRisks);
            Assert.Equal(100500, policy.Premium);
        }

        [Theory]
        [MemberData(nameof(FullConstructorParams))]
        public void TestFullCtorFailure(string name, DateTime fromDate, DateTime tillDate, decimal premium,
            IList<Risk> risks)
        {
            Assert.ThrowsAny<ArgumentException>(() =>
                new InsuranceCompany.Policy.Policy(name, fromDate, tillDate, premium, risks));
        }

        public static IEnumerable<object[]> InitializedConstructorParams => new List<object[]>
        {
            new object[]
            {
                null, DateTime.UtcNow, DateTimeOffset.UtcNow.AddDays(1).DateTime, new List<Risk>()
            },
            new object[]
            {
                string.Empty, DateTime.UtcNow, DateTimeOffset.UtcNow.Subtract(TimeSpan.FromDays(2)), new List<Risk>()
            },
            new object[]
            {
                string.Empty, DateTime.UtcNow, DateTimeOffset.UtcNow.AddDays(1).DateTime, null
            }
        };

        public static IEnumerable<object[]> FullConstructorParams => new List<object[]>
        {
            new object[]
            {
                null, DateTime.UtcNow, DateTimeOffset.UtcNow.AddDays(1).DateTime, 100, new List<Risk>()
            },
            new object[]
            {
                string.Empty, DateTimeOffset.UtcNow.AddDays(1).DateTime, DateTime.UtcNow, 100, new List<Risk>()
            },
            new object[]
            {
                string.Empty, DateTime.UtcNow, DateTimeOffset.UtcNow.AddDays(1).DateTime, -100, new List<Risk>()
            },
            new object[]
            {
                string.Empty, DateTime.UtcNow, DateTimeOffset.UtcNow.AddDays(1).DateTime, 100, null
            }
        };
    }
}