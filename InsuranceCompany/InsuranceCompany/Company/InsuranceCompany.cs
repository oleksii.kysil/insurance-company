using System;
using System.Collections.Generic;
using System.Linq;
using InsuranceCompany.Models;
using InsuranceCompany.Policy;

namespace InsuranceCompany.Company
{
    public class InsuranceCompany : IInsuranceCompany
    {
        private readonly IDictionary<string, IList<(Risk risk, DateTime effectiveFrom, DateTime effectiveTo)>>
            _risksEffectiveDates;

        private readonly IDictionary<string, IList<IPolicy>> _policies;
        private const int NumberOfDaysPerYear = 365;

        public InsuranceCompany(string name) : this(name, new List<Risk>())
        {
        }

        public InsuranceCompany(string name, IList<Risk> availableRisks)
        {
            if (string.IsNullOrWhiteSpace(name)) throw new ArgumentException("Company name cannot be empty");

            Name = name;
            AvailableRisks = availableRisks;
            _risksEffectiveDates =
                new Dictionary<string, IList<(Risk risk, DateTime effectiveFrom, DateTime effectiveTo)>>();
            _policies = new Dictionary<string, IList<IPolicy>>();
        }

        public string Name { get; }
        public IList<Risk> AvailableRisks { get; set; }

        public IPolicy SellPolicy(string nameOfInsuredObject, DateTime validFrom, short validMonths,
            IList<Risk> selectedRisks)
        {
            if (validFrom < DateTime.UtcNow) throw new ArgumentException("Start date is in the past");

            if (validMonths <= 0) throw new ArgumentException("Policy should be valid for at least 1 month");

            if (selectedRisks.Intersect(AvailableRisks).Count() != selectedRisks.Count)
                throw new ArgumentException("Submitted selected risks are not available in the company");

            var validTill = new DateTimeOffset(validFrom).AddMonths(validMonths).DateTime;
            if (_policies.ContainsKey(nameOfInsuredObject) && _policies[nameOfInsuredObject]
                    .Any(x => x.ValidFrom < validTill && validFrom < x.ValidTill))
                throw new ArgumentException($"Policy for object {nameOfInsuredObject} already exist for period");
            var policy = new Policy.Policy(nameOfInsuredObject, validFrom, validTill, selectedRisks);
            AddRisks(selectedRisks, validFrom, validTill, nameOfInsuredObject);
            policy.Premium = CalculatePolicyPremium(policy, _risksEffectiveDates[nameOfInsuredObject]);
            AddPolicy(nameOfInsuredObject, policy);
            return policy;
        }

        public void AddRisk(string nameOfInsuredObject, Risk risk, DateTime validFrom, DateTime effectiveDate)
        {
            var policy = GetPolicy(nameOfInsuredObject, effectiveDate);
            if (policy == null) throw new ArgumentException("Invalid data. No policy found for object and period");

            if (validFrom < DateTime.UtcNow) throw new ArgumentException("From date is invalid");

            if (!AvailableRisks.Any(x => x.Name == risk.Name && x.YearlyPrice == risk.YearlyPrice))
                throw new ArgumentException("Invalid risk submitted");

            AddRisks(new List<Risk> {risk}, validFrom, policy.ValidTill, nameOfInsuredObject);
            policy.InsuredRisks.Add(risk);
            policy.Premium = CalculatePolicyPremium(policy, _risksEffectiveDates[nameOfInsuredObject]);
        }

        public void RemoveRisk(string nameOfInsuredObject, Risk risk, DateTime validTill, DateTime effectiveDate)
        {
            var policy = GetPolicy(nameOfInsuredObject, effectiveDate);
            if (policy == null) throw new ArgumentException("Invalid data. No policy found for object and period");

            (Risk risk, DateTime effectiveFrom, DateTime effectiveTo) currentRisk;
            try
            {
                currentRisk = _risksEffectiveDates[nameOfInsuredObject]
                    .First(x => x.risk.Name == risk.Name && x.risk.YearlyPrice == risk.YearlyPrice);
            }
            catch (InvalidOperationException)
            {
                throw new ArgumentException("No submitted risk found");
            }

            if (currentRisk.effectiveFrom > validTill) throw new ArgumentException("Invalid end date");

            policy.InsuredRisks.Remove(risk);
            _risksEffectiveDates[nameOfInsuredObject].Remove(currentRisk);
            policy.Premium = CalculatePolicyPremium(policy, _risksEffectiveDates[nameOfInsuredObject]);
        }

        public IPolicy GetPolicy(string nameOfInsuredObject, DateTime effectiveDate)
        {
            if (nameOfInsuredObject == null || !_policies.ContainsKey(nameOfInsuredObject))
                throw new ArgumentException("Name of object is not defined and unknown by company");

            return _policies[nameOfInsuredObject]
                .FirstOrDefault(x => effectiveDate > x.ValidFrom && effectiveDate < x.ValidTill);
        }

        private static decimal CalculatePolicyPremium(IPolicy policy,
            IEnumerable<(Risk risk, DateTime effectiveFrom, DateTime effectiveTo)> risks)
        {
            var startDate = policy.ValidFrom;
            var endDate = policy.ValidTill;
            var effectiveRisks = risks.Where(x => x.effectiveFrom >= startDate).ToList();

            return (from risk in effectiveRisks
                let endRiskDate = new DateTime(Math.Min(endDate.Ticks, risk.effectiveTo.Ticks))
                select risk.risk.YearlyPrice *
                       decimal.Divide((endRiskDate - risk.effectiveFrom).Days, NumberOfDaysPerYear)).Sum();
        }

        private void AddRisks(IEnumerable<Risk> risks, DateTime validFrom, DateTime validTill, string objectName)
        {
            foreach (var risk in risks)
            {
                if (!_risksEffectiveDates.ContainsKey(objectName))
                {
                    _risksEffectiveDates[objectName] =
                        new List<(Risk risk, DateTime effectiveFrom, DateTime effectiveTo)>
                            {(risk, validFrom, validTill)};
                    continue;
                }

                _risksEffectiveDates[objectName].Add((risk, validFrom, validTill));
            }
        }

        private void AddPolicy(string objectName, IPolicy policy)
        {
            if (!_policies.ContainsKey(objectName))
            {
                _policies[objectName] = new List<IPolicy> {policy};
                return;
            }

            _policies[objectName].Add(policy);
        }
    }
}