using System;
using System.Collections.Generic;
using InsuranceCompany.Models;

namespace InsuranceCompany.Policy
{
    public class Policy : IPolicy
    {
        private const int DefaultPeriodDaysCount = 1;
        private const int DefaultPremium = 0;

        public Policy() : this(string.Empty, DateTime.UtcNow,
            DateTimeOffset.UtcNow.AddDays(DefaultPeriodDaysCount).DateTime,
            new List<Risk>())
        {
        }

        public Policy(string nameOfInsuredObject, DateTime validFrom, DateTime validTill, IList<Risk> risks) : this(
            nameOfInsuredObject,
            validFrom, validTill, DefaultPremium, risks)
        {
        }

        public Policy(string nameOfInsuredObject, DateTime validFrom, DateTime validTill, decimal premium,
            IList<Risk> insuredRisks)
        {
            if (validFrom > validTill) throw new ArgumentException("Invalid dates for policy");

            if (premium < 0) throw new ArgumentException("Premium should not be negative");

            NameOfInsuredObject = nameOfInsuredObject ?? throw new ArgumentNullException(nameof(nameOfInsuredObject));
            ValidFrom = validFrom;
            ValidTill = validTill;
            InsuredRisks = insuredRisks ?? throw new ArgumentException("Policy requires risks");
            Premium = premium;
        }

        public string NameOfInsuredObject { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTill { get; set; }
        public decimal Premium { get; set; }
        public IList<Risk> InsuredRisks { get; set; }
    }
}